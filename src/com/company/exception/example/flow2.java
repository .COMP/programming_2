package com.company.exception.example;

public class flow2 {
    public static void main(String[] args) {

        // array of size 4.
        int[] array = new int[2];

        try {
            int i = array[10];

            // this statement will never execute
            // as exception is raised by above statement
            System.out.println("Inside try block");
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Exception caught in catch block");
        } finally {
            System.out.println("finally block executed");
        }

        // rest program will be executed
        System.out.println("Outside try-catch-finally clause");
    }
}
