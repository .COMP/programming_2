package com.company.exception.example;

public class flow1 {
    public static void main(String[] args) {
        // array of size 2.
        int[] array = new int[2];
        try {
            int i = array[10];
            // this statement will never execute
            // as exception is raised by above statement
            System.out.println("Inside try block");
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Exception caught in Catch block");
        }
        // rest program will be executed
        System.out.println("Outside try-catch clause");
    }
}
