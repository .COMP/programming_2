package com.company.exception.example;

public class flow7 {
    public static void main(String[] args) {

        // array of size 4.
        int[] array = new int[2];
        try {
            int i = array[10];

            // this statement will never execute
            // as exception is raised by above statement
            System.out.println("Inside try block");
        } finally {
            System.out.println("finally block executed");
        }

        // rest program will not execute
        System.out.println("Outside try-finally clause");
    }
}
