package com.company.exception;

public class ExceptionSyntax {
    public static void main(String[] args) {
        ExceptionSyntax exceptionSyntax = new ExceptionSyntax();
        exceptionSyntax.printResult();
    }

    public void printResult(){
        try {
            // This statement throw exception
            System.out.println(1/0);
        }catch (ArithmeticException ex){
            System.out.println("Arithmetic exception caught");
        }

//        finally {
//            System.out.println("Finally");
//        }
    }
}
