package com.company.abstraction.shape_example;

abstract public class Shape {
    String color;

    public Shape(String color) {
        System.out.println("Shape constructor called");
        this.color = color;
    }

    // Abstract method's
    abstract double area();

    public abstract String toString();

    // Concrete method's
    public String getColor() {
        return color;
    }

}
