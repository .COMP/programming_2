package com.company.abstraction.abstraction_rbcs_example;

abstract public class RbcsTeamAbstract {
    abstract void collectInformation();

    abstract void writeContent();

}
