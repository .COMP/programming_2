package com.company.abstraction.abstraction_rbcs_example;

public class ProgrammingTeam extends RbcsTeamAbstract{
    @Override
    void collectInformation() {
        System.out.println("Collect programming information ");
    }

    @Override
    void writeContent() {
        System.out.println("Write programming information ");
    }
}

