package com.company.abstraction.abstraction_rbcs_example;

public class AutomataTeam extends RbcsTeamAbstract{
    @Override
    void collectInformation() {
        System.out.println("Collect automata information ");
    }

    @Override
    void writeContent() {
        System.out.println("Write automata information ");
    }
}

