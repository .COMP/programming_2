package com.company.interfaces.interfacesrbcs_example;

public class MathsTeam implements RbcsTeamInterface {
    @Override
    public void collectInformation() {
        System.out.println("Collect information from maths book's" +
                "and collect exercise's from different source");
    }

    @Override
    public void writeContent() {
        System.out.println("write content by maths team and write exercise");
    }
}
