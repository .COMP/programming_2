package com.company.interfaces.interfacesrbcs_example;

public class ProgrammingTeam implements RbcsTeamInterface {
    @Override
    public void collectInformation() {
        System.out.println("Collect information from programming book's");
    }

    @Override
    public void writeContent() {
        System.out.println("write content by programming team");
    }
}
