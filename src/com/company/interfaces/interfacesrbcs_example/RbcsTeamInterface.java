package com.company.interfaces.interfacesrbcs_example;

public interface RbcsTeamInterface  extends  BinaryTeam {
    void collectInformation();

    void writeContent();

    default void Audit() {
        System.out.println("Auditing Content by same way between all team's");
    }
}
